@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <div>
                            Доступность данного метода: {{$check == 1 ? 'доступен' : ($check === '' ? 'Выберите permission' : 'недоступен')}}
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-10 col-md-offset-1">
                            <form id="new_form_messages" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Название:</label>

                                    <select name="name" class="form-control">
                                        @foreach($metods as $metod)
                                            <option value="{{$metod->name}}">{{$metod->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-default">Проверить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
