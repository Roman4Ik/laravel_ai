<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('auth_item')->insert([
            [
                'name' => 'admin',
                'type' => 1, //роль
                'description' => 'роль для админов',
            ],
            [
                'name' => 'user',
                'type' => 1, //роль
                'description' => 'роль для пользователей',
            ],
            [
                'name' => 'view_admin',
                'type' => 2, //разрешение
                'description' => 'разрешение на просмотр админки',
            ],
            [
                'name' => 'update_admin',
                'type' => 2, //разрешение
                'description' => 'разрешение на редактирование админки',
            ],
        ]);

        //связываем разрешение с ролью
        DB::table('auth_item_child')->insert([
            [
                'parent' => 'admin',
                'child' => 'view_admin',
            ],
            [
                'parent' => 'admin',
                'child' => 'update_admin',
            ],
        ]);

        //связываем пользователя с ролью
        DB::table('auth_assignment')->insert([
            [
                'item_name' => 'admin',
                'user_id' => 1,
            ],
            [
                'item_name' => 'user',
                'user_id' => 2,
            ],
        ]);
    }
}
