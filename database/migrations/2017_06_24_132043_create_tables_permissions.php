<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('auth_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->integer('type');
            $table->text('description');
            $table->timestamps();
        });


        Schema::create('auth_item_child', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parent', 64);
            $table->string('child', 64);
            $table->timestamps();
        });


        Schema::create('auth_assignment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_name', 64);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_assignment');
        Schema::dropIfExists('auth_item_child');
        Schema::dropIfExists('auth_item');
    }
}
