<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthItem extends Model
{
    protected $table = 'auth_item';

    static $T_ROLE = 1;         //роли
    static $T_PERMISSION = 2;   //разрешиния
}
