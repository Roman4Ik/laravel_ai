<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthAssignment extends Model
{
    protected $table = 'auth_assignment';
}
