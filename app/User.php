<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Symfony\Component\VarDumper\VarDumper;

class User extends Authenticatable
{
    public $_assignments;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAssignments()
    {
        return AuthAssignment::where('user_id', $this->id)
            ->select('item_name')
            ->get()
            ->toArray();
    }

    public static function check_item_auth($auth, $permission)
    {
        foreach ($auth as $item) {
            if ($item['item_name'] == $permission) {
                return true;
            }
        }
        return false;
    }


    public static function check(self $user, $permission)
    {
        if (!isset($user->_assignments))
            $user->_assignments = $user->getAssignments();


        if (self::check_item_auth($user->_assignments, $permission)) {
            return true;
        }

        $parents = AuthItemChild::where(['child' => $permission])
            ->select(['parent'])
            ->get()
            ->toArray();

        foreach ($parents as $parent) {
            if (self::check($user, $parent['parent'])) {
                return true;
            }
        }


        return false;
    }
}
