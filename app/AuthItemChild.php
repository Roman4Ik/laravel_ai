<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthItemChild extends Model
{
    protected $table = 'auth_item_child';
}
