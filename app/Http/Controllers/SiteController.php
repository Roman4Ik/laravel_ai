<?php

namespace App\Http\Controllers;

use App\AuthItem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $check = '';
        if ($request->input('name')) {
            $check = User::check(Auth::user(), $request->input('name'));
        }

        $metods = AuthItem::where(['type' => AuthItem::$T_PERMISSION])->get();
        return view('site.index', [
            'check' => $check,
            'metods' => $metods,
        ]);
    }
}
